package com.example.server.Controllers

import com.example.server.Models.Result
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class MathsController {

    @GetMapping("/add")
    fun add(@RequestParam(value="firstNumber",defaultValue = "0") firstNumber: Int, @RequestParam(value = "secondNumber",defaultValue = "0") secondNumber: Int)=
            Result(firstNumber+secondNumber)

    @GetMapping("/subtract")
    fun subtract(@RequestParam(value="firstNumber",defaultValue = "0") firstNumber: Int, @RequestParam(value = "secondNumber",defaultValue = "0") secondNumber: Int)=
            Result(firstNumber-secondNumber)

    @GetMapping("/multiply")
    fun multiply(@RequestParam(value="firstNumber",defaultValue = "0") firstNumber: Int, @RequestParam(value = "secondNumber",defaultValue = "0") secondNumber: Int)=
            Result(firstNumber*secondNumber)

    @GetMapping("/divide")
    fun divide(@RequestParam(value="firstNumber",defaultValue = "0") firstNumber:  Float, @RequestParam(value = "secondNumber",defaultValue = "1") secondNumber: Float)=
            Result(firstNumber/secondNumber)

}