package com.example.server.Controllers

import com.example.server.Models.Greeting
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.atomic.AtomicLong

/* Run This to start server must be at the top level of the directory */
@RestController
class GreetingController {

    val counter = AtomicLong()

    @GetMapping("/greeting")
    fun greeting(@RequestParam(value = "name", defaultValue = "World") name: String)=
            Greeting(counter.incrementAndGet(),"Helllo, $name")

}