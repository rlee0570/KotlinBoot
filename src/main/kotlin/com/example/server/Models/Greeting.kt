package com.example.server.Models

data class Greeting(val id: Long, val content: String)